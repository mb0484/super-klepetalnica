/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  var jedregljaj = sporocilo.indexOf("&#9756") > -1;
  if (jedregljaj) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  }
  var jeslika = sporocilo.indexOf("http") > -1 && (sporocilo.indexOf(".jpg") > -1 || sporocilo.indexOf(".png") > -1 || sporocilo.indexOf(".gif") > -1);
  if (jeslika) {
    //console.log("je slika");
    return divHtmlElementTextZaSliko(sporocilo);
  }
  if (jeSmesko) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } else {
    //console.log("nekaj le je2");
    return $('<div style="font-weight: bold"></div>').text(sporocilo);
  }
}

function divHtmlElementTextZaSliko(sporocilo) {
  var vmesnospr = "";
  vmesnospr += sporocilo;
  var razdeljenosporocilo = sporocilo.split(' ');
  var tabelaindeksov = [];
  var j = 0;
  for (var i = 0; i < razdeljenosporocilo.length; ++i) {
    if (i != 0) {
      //vmesnospr += " ";
    }
    //console.log(razdeljenosporocilo[i]);
    if (razdeljenosporocilo[i].indexOf("http") == 0 && (razdeljenosporocilo[i].indexOf(".jpg") > -1 || razdeljenosporocilo[i].indexOf(".png") > -1 || razdeljenosporocilo[i].indexOf(".gif") > -1)) {
      //vmesnospr += "<br /><html><img src=\"" + razdeljenosporocilo[i] + "\" style=\"width:200px; margin-left:20px;\"\"></html><br />";
      tabelaindeksov[j] = i;
      j++;
    }
    //vmesnospr += razdeljenosporocilo[i];
  }
  //vmesnospr = razdeljenosporocilo.join(" ");
  for (var i = 0; i < tabelaindeksov.length; ++i) {
    if (razdeljenosporocilo[tabelaindeksov[i]][razdeljenosporocilo[tabelaindeksov[i]].length - 1] == ".") {
      razdeljenosporocilo[tabelaindeksov[i]] = razdeljenosporocilo[tabelaindeksov[i]].substring(0, razdeljenosporocilo[tabelaindeksov[i]].length - 1);
      //console.log("je pika");
    }
    
    vmesnospr += "<br /><html><img src=\"" + razdeljenosporocilo[tabelaindeksov[i]] + "\" style=\"width:200px; margin-left:20px;\"\"></html><br />";
  }
  return vmesnospr;
}



/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementEnostavniTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementEnostavniTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var sejepreimenoval = 0;
    if (sporocilo.besedilo.indexOf("se je preimenoval v") > -1) {
      sejepreimenoval = 1;
    }
    var besede = sporocilo.besedilo.split(' ');
    //console.log(besede[0]);
    var dodajpodpicje = 0, sejespremenilo = 0;
    // to se izvede, ce se je oseba preimenovala
    if (sejepreimenoval == 1) {
      //console.log(besede[0]);
      for (var key in zamenjevalnatabela) {
        if (besede[0] == zamenjevalnatabela[key].key) {
          zamenjevalnatabela[key].key = besede[5].replace(".", '');
          besede[0] = zamenjevalnatabela[key].value + " (" + besede[0].replace(":", '') + "})";
        }
      }
    }
    
    if (besede[0].indexOf(":") > -1) {
      dodajpodpicje = 1;
    }
    for (var key in zamenjevalnatabela) {
      if (besede[0].replace(":", '') == zamenjevalnatabela[key].key) {
          besede[0] = zamenjevalnatabela[key].value + " (" + besede[0].replace(":", '') + ")";
          sejespremenilo = 1;
      }
    }
    if (dodajpodpicje == 1 && sejespremenilo == 1) {
      besede[0] += ":";
    }
    sporocilo.besedilo = besede.join(' ');
    var novElement = divElementEnostavniTekst(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    for (var i=0; i < uporabniki.length; i++) {
      //$('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
      //console.log("nekaj le je");
      var sejezeispisalo = 0;
      for (var key in zamenjevalnatabela) {
        //console.log(zamenjevalnatabela[key].key);
        if (uporabniki[i] == zamenjevalnatabela[key].key) {
          $('#seznam-uporabnikov').append(divElementEnostavniTekst(zamenjevalnatabela[key].value + " (" + uporabniki[i] + ")"));
          sejezeispisalo = 1;
        }
      }
      if (sejezeispisalo == 0)
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
    }
    $('#seznam-uporabnikov div').click(function() {
      //console.log("nekaj le je");
      //var sporocilo = trenutniKanal + "lep je dan";
      $('#sporocila').append(divElementEnostavniTekst("(zasebno za " + $(this).text() + "): " + "&#9756"));
      var uporabnik = $(this).text();
      if (uporabnik.indexOf("(") > -1) {
        uporabnik = uporabnik.split(' ');
        uporabnik = uporabnik[1];
        uporabnik = uporabnik.replace("(", '');
        uporabnik = uporabnik.replace(")", '');
      }
      klepetApp.procesirajUkaz('/zasebno ' + "\"" + uporabnik + "\"" + "\"" +"&#9756" + "\"");
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});